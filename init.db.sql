DO
$do$
    DECLARE
  DB_NAME VARCHAR(50);
BEGIN
    DB_NAME = 'bookingupbook';
    create extension if not exists dblink;
   IF EXISTS (SELECT FROM pg_database WHERE datname = DB_NAME) THEN
      RAISE NOTICE 'Database already exists';  -- optional
   ELSE
      PERFORM dblink_exec('dbname=' || current_database(), 'CREATE DATABASE ' ||  DB_NAME || ';');
      PERFORM dblink_exec('dbname= ' || DB_NAME, 'create extension "uuid-ossp"');
   END IF;
END
$do$;
