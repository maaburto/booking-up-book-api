'use strict';

/** @type {import('sequelize-cli').Migration} */
module.exports = {
  async up(queryInterface, Sequelize) {
    return queryInterface.bulkInsert('Students', [
      {
        first_name: 'Elena',
        last_name: 'Devoir',
        email: 'elena@supreme-books.com',
        date_of_birth: '1980-11-06',
      },
      {
        first_name: 'Mauricio',
        last_name: 'Devero',
        email: 'mau@invur-college.edu.com',
        date_of_birth: '1980-01-06',
      },
      {
        first_name: 'Moises',
        last_name: 'Aburto',
        email: 'maburto@example-email.com',
        date_of_birth: '1980-01-06',
      },
    ]);
  },

  async down(queryInterface, Sequelize) {
    return queryInterface.bulkDelete('Students', null, {});
  },
};
