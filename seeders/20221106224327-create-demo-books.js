'use strict';

/** @type {import('sequelize-cli').Migration} */
module.exports = {
  async up (queryInterface, Sequelize) {
    return queryInterface.bulkInsert('Books', [
      {
        name: 'Book 1',
        number_of_pages: 340,
        published_at: '1840-10-10',
        book_description: `While Hardy wrote poetry throughout his life and regarded himself primarily as a poet,
          his first collection was not published until 1898. Initially, he gained fame as the author of novels such
          as Far from the Madding Crowd (1874), The Mayor of Casterbridge (1886), Tess of the d'Urbervilles (1891),
          and Jude the Obscure (1895). During his lifetime, Hardy's poetry was acclaimed by younger poets (particularly
            the Georgians) who viewed him as a mentor. After his death his poems were lauded`,
      },
      {
        name: 'Book 2',
        number_of_pages: 290,
        published_at: '1780-11-10',
        book_description: `ather John Banister Tabb (March 22, 1845 - November 19, 1909) was an American poet,
        Roman Catholic priest, and professor of English.`,
      },
      {
        name: 'Alice in Wonderland',
        number_of_pages: 280,
        published_at: '1966-10-10',
        book_description: `Alice's Adventures in Wonderland is an 1865 novel written by English author Charles Lutwidge
        Dodgson over the pseudonym Lewis Carroll.`,
      },
      {
        name: 'Fantastic Mr Fox',
        number_of_pages: 230,
        published_at: '1960-01-08',
        book_description: `Mr Fox steals food from the horrible farmers Boggis, Bunce and Bean - one fat, one short, one lean.
          These three crooks concoct plan to dig Mr Fox out of his home, but they don't realize how truly fantastic Mr Fox is,
          or how far he'll go to save his family`,
      },
    ]);
  },

  async down (queryInterface, Sequelize) {
    return queryInterface.bulkDelete('Books', null, {});
  }
};
