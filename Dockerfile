FROM node:18-alpine

# Create app directory
RUN mkdir -p /usr/src/app

# Install app dependencies
COPY package.json /usr/src/app/
WORKDIR /usr/src/app

# Mapping mount point
# this mount point overwrites the node_modules folder from the host
VOLUME /usr/src/app/node_modules

# Bundle app source
COPY . /usr/src/app

EXPOSE 3000
ENV PORT 3000

CMD [ "npm", "install" ]

