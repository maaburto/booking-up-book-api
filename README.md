# booking-up-book-api



## Getting started

### Install Packages

```
npm install
```

### Create Database environment
In your favorite SQL Manager IDE, apply `init.db.sql` file

***

## App Environment Variables
Make a copy from `.env.example` to `.env`

In the file (`.env.example`), you will find all environment variables related to project

in `.env` file add the corresponding values according project and your host/own preferences

```
PORT=[SERVER_PORT AS NUMBER] // PORT=3000 
DB_CONNECTION=[DB CONNECTION IN STRING FORMAT AS STRING] // postgres://[user-postgres]:[password-postgres]@[ip-or-hostname]:[port-postgres]/bookingupbook
ROOT_GRAPHQL=[ROOT ROUTE TO REACHT GRAPHQL MATTERS AS STRING]

```

#### ***Note***: DB_CONNECTION case, please note `bookingupbook` needs to be on there because that's the DB name, we need to pointing out.

## Start App (Now only dev is available)
```
npm run start:dev
```


***
# Extra Options

## Seeds
In order to create demo data samples for Database, apply the following command:

```
npx sequelize-cli db:seed:all
```

#### ***Note*** : To apply seeders successfully, you should to replace the correct values (right Database's username/password/host/port) under `config.json` file. That's located on `/config/config.json`.

#### ***Note*** : First you should have to applied `npm run start:dev` in order to create the core tables and could apply the above seeder command.

##  Get Latest GraphQL Queries & Mutations

Import in your Postman App the following file

`booking-up-books.postman_collection.json`

#### ***Note***: Above file is located in the root of project.
