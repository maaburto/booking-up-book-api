import { Book } from '../../book/core';

export interface Student {
  id: string;
  firstName: string;
  lastName: number;
  email: string;
  dateOfBirth?: string;
  books: Book[];
}
