import { NewStudentDto } from './student.dto';
import { Student } from './student.entity';

export interface StudentRepositoryContract {
  findById(studentId: string): Promise<Student | null>;
  findAll(): Promise<Student[]>;
  addOne(data: NewStudentDto): Promise<Student>;
  assignBook(bookId: string, studentId: string): Promise<boolean | undefined>;
}
