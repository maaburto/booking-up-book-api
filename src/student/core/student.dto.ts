export interface NewStudentDto {
  firstName: string;
  lastName: number;
  email: string;
  dateOfBirth?: string;
}
