import Joi from '@hapi/joi';
import { dateValidation } from '../../shared-kernel/core';

const firstName = Joi.string().max(255).required().label('firstName');
const lastName = Joi.string().max(255).required().label('lastName');
const email = Joi.string().email().label('email');
const dateOfBirth = dateValidation.label('dateOfBirth');

export const newStudentValidation = Joi.object().keys({
  firstName,
  lastName,
  dateOfBirth,
  email,
});

export const bookIdValidation = Joi.string().guid({ version: 'uuidv4' }).required().label('bookId');
export const studentIdValidation = Joi.string().guid({ version: 'uuidv4' }).required().label('bookId');
