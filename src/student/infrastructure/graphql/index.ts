export { default as studentTypeDef } from './student.graphql';

import { StudentFeatures, StudentBookFeatures } from '../../features';
import { StudentRepository } from '../repository';
import { StudentResolver } from './student.resolver';


const studentRepository = new StudentRepository();
const studentBookFeatures = new StudentBookFeatures(studentRepository);
const studentFeatures = new StudentFeatures(studentRepository);
const studentResolver = new StudentResolver(studentFeatures, studentBookFeatures);

// Once Dependencies has been fulfilled, it's time to run the route
const studentResolve = studentResolver.resolve();
export { studentResolve };
