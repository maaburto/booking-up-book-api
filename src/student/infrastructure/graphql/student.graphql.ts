import { gql } from 'apollo-server-express';

export default gql`
  extend type Query {
    students: [Student!]!
    student(id: ID!): Student
  }

  extend type Mutation {
    addStudent(firstName: String!, lastName: String!, email: String!, dateOfBirth: String): Student!
    deleteStudent(id: ID!): Boolean!
    assignBook(bookId: String!, studentId: String!): Boolean!
  }

  type Student {
    id: String!
    firstName: String!
    lastName: String!
    email: String!
    dateOfBirth: String
    books: [Book!]!
  }
`;
