import { Book } from '../../../book/core';
import { NewStudentDto, Student } from '../../core';
import { StudentFeatures, StudentBookFeatures } from '../../features';

export class StudentResolver {
  constructor(private studentFeatures: StudentFeatures, private studentBookFeatures: StudentBookFeatures) {}

  private getOneStudent(studentId: string): Promise<Student | null> {
    return this.studentFeatures.getOneStudent(studentId);
  }

  private getStudents(): Promise<Student[]> {
    return this.studentFeatures.getStudents();
  }

  private addStudent(newCommittedStudent: NewStudentDto): Promise<Student> {
    return this.studentFeatures.addStudent(newCommittedStudent);
  }

  private deleteStudent(bookId: string): Promise<boolean> {
    return this.studentFeatures.deleteStudent(bookId);
  }

  private assignBookToStudent(bookId: string, studentId: string): Promise<boolean | undefined> {
    return this.studentBookFeatures.assignBook(bookId, studentId);
  }

  private async getBooksStudent(studentId: string): Promise<Book[]> {
    return this.studentBookFeatures.getBooksStudent(studentId);
  }

  resolve() {
    return {
      Query: {
        student: async (_: any, { id }: { id: string }, context: any) => {
          const student = await this.getOneStudent(id);
          return student;
        },
        students: async () => {
          const students = await this.getStudents();
          return students;
        },
      },

      Mutation: {
        addStudent: async (_: any, args: NewStudentDto, context: any) => await this.addStudent(args),
        deleteStudent: async (_: any, { id }: { id: string }) => await this.deleteStudent(id),
        assignBook: async (_: any, { bookId, studentId }: { bookId: string; studentId: string }) =>
          await this.assignBookToStudent(bookId, studentId),
      },

      Student: {
        books: async ({ id }: { id: string }) => {
          return await this.getBooksStudent(id);
        },
      },
    };
  }
}
