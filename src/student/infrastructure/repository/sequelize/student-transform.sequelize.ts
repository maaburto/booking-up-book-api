import { Model } from 'sequelize';
import { Student } from '../../../core';
import { StudentDBEntity } from './student.schema';

export const parseSequelizeDBEntityToStudent = (studentModel: Model<StudentDBEntity, any>): Student => {
  const currentValue = studentModel.get();

  const student: Student = {
    id: currentValue.id,
    firstName: currentValue.first_name,
    lastName: currentValue.last_name,
    email: currentValue.email,
    dateOfBirth: currentValue.date_of_birth,
    books: [],
  };

  return student;
};

export const parseSequelizeDBEntityToStudents = (studentsModel: Model<StudentDBEntity, any>[]): Student[] => {
  return studentsModel.map((studentModel) => parseSequelizeDBEntityToStudent(studentModel));
};
