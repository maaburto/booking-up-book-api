import { Student, NewStudentDto } from '../../../core';
import { StudentRepositoryContract } from '../../../core';
import { BookSchemaModel, BooksStudentsSchemaModel, StudentSchemaModel } from '../../../../shared-kernel';
import { Book } from '../../../../book/core';
import {
  parseSequelizeDBEntityToStudent,
  parseSequelizeDBEntityToStudents,
} from './student-transform.sequelize';
import { parseSequelizeBookDBEntityToBooks } from '../../../../book/infrastructure/repository';
import sequelize from '../../../../shared-kernel/infrastructure/sequilize/bootstrapping.db';

export class SequelizeStudentRepository implements StudentRepositoryContract {
  async findById(studentId: string): Promise<Student | null> {
    const student = await StudentSchemaModel.findOne({
      where: {
        id: studentId,
      },
      include: BookSchemaModel,
    });

    if (student) {
      return parseSequelizeDBEntityToStudent(student);
    }

    return null;
  }

  async findAll(): Promise<Student[]> {
    const students = await StudentSchemaModel.findAll();
    return parseSequelizeDBEntityToStudents(students);
  }

  async addOne(student: NewStudentDto): Promise<Student> {
    const newStudent = await StudentSchemaModel.create({
      first_name: student.firstName,
      last_name: student.lastName,
      email: student.email,
      date_of_birth: student.dateOfBirth,
    });

    return parseSequelizeDBEntityToStudent(newStudent);
  }

  async deleteOne(studentId: string): Promise<boolean> {
    const deleted = await StudentSchemaModel.destroy({
      where: {
        id: studentId,
      },
    });

    return !!deleted;
  }

  async assignBook(bookId: string, studentId: string): Promise<boolean | undefined> {
    try {
      const result = sequelize.transaction(async (t) => {
        const booksPerStudent = await BooksStudentsSchemaModel.findAll({
          where: {
            StudentId: studentId,
          },
        });

        if (booksPerStudent.length === 3) {
          throw new Error('Only 3 books per Student');
        }

        const newAssignment = await BooksStudentsSchemaModel.create(
          {
            BookId: bookId,
            StudentId: studentId,
          },
          { transaction: t }
        );

        return !!newAssignment.get();
      });

      return result;

      // If the execution reaches this line, the transaction has been committed successfully
      // `result` is whatever was returned from the transaction callback (the `user`, in this case)
    } catch (error) {
      // If the execution reaches this line, an error occurred.
      // The transaction has already been rolled back automatically by Sequelize!
      throw error;
    }
  }

  async getBooksById(studentId: string): Promise<Book[]> {
    const books = await BookSchemaModel.findAll({
      include: [
        {
          model: StudentSchemaModel,
          where: {
            id: studentId,
          },
        },
      ],
    });

    return parseSequelizeBookDBEntityToBooks(books);
  }
}
