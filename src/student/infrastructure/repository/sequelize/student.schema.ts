import { Sequelize, DataTypes, ModelAttributes, Model } from 'sequelize';

export interface StudentDBEntity {
  id: string;
  first_name: string;
  last_name: number;
  email: string;
  date_of_birth: string;
  createdAt: string;
  updatedAt: string;
}

export const studentAttributes: ModelAttributes<Model<StudentDBEntity, any>> = {
  // Model attributes are defined here
  id: {
    type: DataTypes.UUID,
    primaryKey: true,
    allowNull: false,
    defaultValue: Sequelize.literal('uuid_generate_v4()'),
  },
  first_name: {
    type: DataTypes.STRING,
    allowNull: false,
  },
  last_name: {
    type: DataTypes.STRING,
    allowNull: false,
  },
  email: {
    type: DataTypes.STRING,
    allowNull: false,
    validate: {
      isEmail: true,
    },
  },
  date_of_birth: {
    type: DataTypes.DATEONLY,
    allowNull: true,
  },
  createdAt: {
    type: DataTypes.DATE,
    defaultValue: Sequelize.literal('NOW()'),
  },
  updatedAt: {
    type: DataTypes.DATE,
    defaultValue: Sequelize.literal('NOW()'),
  },
};

export const studentModelName = 'Student';
