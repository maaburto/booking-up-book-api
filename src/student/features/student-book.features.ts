import { Book } from '../../book/core';
import { studentIdValidation, bookIdValidation } from '../core';
import { StudentRepository } from '../infrastructure/repository';

export class StudentBookFeatures {
  constructor(private readonly studentRepository: StudentRepository) {}

  async getBooksStudent(studentId: string): Promise<Book[]> {
    return this.studentRepository.getBooksById(studentId);
  }

  async assignBook(bookId: string, studentId: string): Promise<boolean | undefined> {
    const bookIdValidated = bookIdValidation.validate(bookId);
    const studentIdValidated = studentIdValidation.validate(studentId);

    if (bookIdValidated.error) {
      throw bookIdValidated.error;
    }

    if (studentIdValidated.error) {
      throw studentIdValidated.error;
    }

    return this.studentRepository.assignBook(bookId, studentId);
  }
}
