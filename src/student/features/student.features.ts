import { Student, NewStudentDto, newStudentValidation, studentIdValidation } from '../core';
import { StudentRepository } from '../infrastructure/repository';

export class StudentFeatures {
  constructor(private readonly studentRepository: StudentRepository) {}

  getOneStudent(studentId: string): Promise<Student | null> {
    return this.studentRepository.findById(studentId);
  }

  async getStudents(): Promise<Student[]> {
    return this.studentRepository.findAll();
  }

  async addStudent(data: NewStudentDto): Promise<Student> {
    const validated = newStudentValidation.validate(data);
    if (validated.error) {
      throw validated.error;
    }

    const newStudent = await this.studentRepository.addOne(data);

    return newStudent;
  }

  async deleteStudent(studentId: string): Promise<boolean> {
    const validated = studentIdValidation.validate(studentId);

    if (validated.error) {
      throw validated.error;
    }

    return this.studentRepository.deleteOne(studentId);
  }
}
