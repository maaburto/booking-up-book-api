import { Student } from '../../student/core';
import { BookRepository } from '../infrastructure/repository';

export class BookStudentFeatures {
  constructor(private readonly bookRepository: BookRepository) {}

  async getStudentsByBookId(bookId: string): Promise<Student[]> {
    return this.bookRepository.getStudentsById(bookId);
  }
}
