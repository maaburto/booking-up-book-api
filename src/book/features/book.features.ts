import { Book, bookIdValidation, NewBookDto, newBookValidation } from '../core';
import { BookRepository } from '../infrastructure/repository';

export class BookFeatures {
  constructor(private readonly bookRepository: BookRepository) {}

  getOneBook(bookId: string): Promise<Book | null> {
    const validated = bookIdValidation.validate(bookId);

    if (validated.error) {
      throw validated.error;
    }

    return this.bookRepository.findById(bookId);
  }

  async getBooks(): Promise<Book[]> {
    return this.bookRepository.findAll();
  }

  async addBook(data: NewBookDto): Promise<Book> {
    const validated = newBookValidation.validate(data);
    if (validated.error) {
      throw validated.error;
    }

    const newBook = await this.bookRepository.addOne(data);

    return newBook;
  }

  async deleteBook(bookId: string): Promise<boolean> {
    const validated = bookIdValidation.validate(bookId);

    if (validated.error) {
      throw validated.error;
    }

    return this.bookRepository.deleteOne(bookId);
  }
}
