import { Student } from '../../student/core';
import { NewBookDto } from './book.dto';
import { Book } from './book.entity';

export interface BookRepositoryContract {
  findById(bookId: string): Promise<Book | null>;
  findAll(): Promise<Book[]>;
  addOne(data: NewBookDto): Promise<Book>;
  deleteOne(bookId: string): Promise<boolean>
  getStudentsById(bookId: string): Promise<Student[]>
}
