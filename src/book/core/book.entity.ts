export interface Book {
  id: string;
  name: string;
  numberOfPages: number;
  publishedAt: string;
  description: string;
}
