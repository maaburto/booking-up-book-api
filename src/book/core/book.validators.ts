import Joi from '@hapi/joi';
import { dateValidation } from '../../shared-kernel/core';

const name = Joi.string().max(255).required().label('name');
const numberOfPages = Joi.number().required().label('numberOfPage');
const publishedAt = dateValidation.required().label('publishedAt');

const description = Joi.string().required().label('description');

export const newBookValidation = Joi.object().keys({
  name,
  numberOfPages,
  publishedAt,
  description,
});

export const bookIdValidation = Joi.string().guid({ version: 'uuidv4' }).required().label('bookId');
