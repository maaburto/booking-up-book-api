export interface NewBookDto {
  name: string;
  numberOfPages: number;
  publishedAt: string;
  description: string;
}
