import { Student } from '../../../student/core';
import { BookRepositoryContract, NewBookDto } from '../../core';
import { Book } from '../../core/book.entity';

export class MongoRepository implements BookRepositoryContract {
  findById(bookId: string): Promise<Book | null> {
    return new Promise((resolve) => resolve(null));
  }

  async findAll(): Promise<Book[]> {
    return [];
  }

  async addOne(book: NewBookDto): Promise<Book> {
    return {
      description: '',
      id: '',
      name: '',
      numberOfPages: 100,
      publishedAt: '',
    };
  }

  async deleteOne(bookId: string): Promise<boolean> {
    return false;
  }

  async getStudentsById(bookId: string): Promise<Student[]> {
    return [];
  }
}
