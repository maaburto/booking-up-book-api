import { Model } from 'sequelize';
import { Book } from '../../../core';
import { BookDBEntity } from './book.schema';

export const parseSequelizeBookDBEntityToBook = (bookModel: Model<BookDBEntity, any>): Book => {
  const currentValue = bookModel.get();

  const book: Book = {
    id: currentValue.id,
    description: currentValue.book_description,
    name: currentValue.name,
    numberOfPages: currentValue.number_of_pages,
    publishedAt: currentValue.published_at,
  };

  return book;
};

export const parseSequelizeBookDBEntityToBooks = (booksModel: Model<BookDBEntity, any>[]): Book[] => {
  return booksModel.map((bookModel) => parseSequelizeBookDBEntityToBook(bookModel));
};
