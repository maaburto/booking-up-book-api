import { Book, NewBookDto } from '../../../core';
import { BookRepositoryContract } from '../../../core';
import { BookSchemaModel, BooksStudentsSchemaModel, StudentSchemaModel } from '../../../../shared-kernel';
import {
  parseSequelizeBookDBEntityToBooks,
  parseSequelizeBookDBEntityToBook,
} from './book-transform.sequelize';
import { Student } from '../../../../student/core';
import { parseSequelizeDBEntityToStudents } from '../../../../student/infrastructure/repository';

export class SequelizeBookRepository implements BookRepositoryContract {
  async findById(bookId: string): Promise<Book | null> {
    const book = await BookSchemaModel.findOne({
      where: {
        id: bookId,
      },
      include: StudentSchemaModel,
    });

    if (book) {
      return parseSequelizeBookDBEntityToBook(book);
    }

    return null;
  }

  async findAll(): Promise<Book[]> {
    const books = await BookSchemaModel.findAll();
    return parseSequelizeBookDBEntityToBooks(books);
  }

  async addOne(book: NewBookDto): Promise<Book> {
    const newBook = await BookSchemaModel.create({
      name: book.name,
      number_of_pages: book.numberOfPages,
      published_at: book.publishedAt,
      book_description: book.description,
    });

    return parseSequelizeBookDBEntityToBook(newBook);
  }

  async deleteOne(bookId: string): Promise<boolean> {
    const deleted = await BookSchemaModel.destroy({
      where: {
        id: bookId,
      },
    });

    return !!deleted;
  }

  async getStudentsById(bookId: string): Promise<Student[]> {
    const students = await StudentSchemaModel.findAll({
      include: [
        {
          model: BookSchemaModel,
          where: {
            id: bookId,
          },
        },
      ],
    });

    return parseSequelizeDBEntityToStudents(students);
  }
}
