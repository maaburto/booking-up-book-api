import { Sequelize, DataTypes, ModelAttributes, Model } from 'sequelize';

export interface BookDBEntity {
  id: string;
  name: string;
  number_of_pages: number;
  published_at: string;
  book_description: string;
  createdAt: string;
  updatedAt: string;
}

export const bookAttributes: ModelAttributes<Model<BookDBEntity, any>> = {
  // Model attributes are defined here
  id: {
    type: DataTypes.UUID,
    primaryKey: true,
    allowNull: false,
    defaultValue: Sequelize.literal('uuid_generate_v4()'),
  },
  name: {
    type: DataTypes.STRING,
    allowNull: false,
  },
  number_of_pages: {
    type: DataTypes.INTEGER,
    allowNull: false,
  },
  published_at: {
    type: DataTypes.DATEONLY,
    allowNull: false,
  },
  book_description: {
    type: DataTypes.TEXT,
    allowNull: false,
  },
  createdAt: {
    type: DataTypes.DATE,
    defaultValue: Sequelize.literal('NOW()'),
  },
  updatedAt: {
    type: DataTypes.DATE,
    defaultValue: Sequelize.literal('NOW()'),
  },
};

export const bookModelName = 'Book';
