import { Router } from 'express';
import { BookFeatures } from '../../features';
import { BookController } from '../controllers';
import { BookRepository } from '../repository';

enum BOOK_ROUTES {
  CREATE = '/book',
  GET = '/book/:id',
  GET_ALL = '/book',
}
const route = Router();
const bookRepository = new BookRepository();
const bookFeatures = new BookFeatures(bookRepository);
const bookController = new BookController(bookFeatures);

// Once Dependencies has been fulfilled, it's time to run the route
route.post(BOOK_ROUTES.GET, bookController.getBook);

export default route;
