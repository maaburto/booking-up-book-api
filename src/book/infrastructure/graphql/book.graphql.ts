import { gql } from 'apollo-server-express';

export default gql`
  extend type Query {
    books: [Book!]!
    book(id: ID!): Book
  }

  input NewBookDtoInput {
    name: String!
    numberOfPages: Int!
    publishedAt: String!
    description: String!
  }

  extend type Mutation {
    addBook(book: NewBookDtoInput): Book!
    deleteBook(id: ID!): Boolean!
  }

  type Book {
    id: ID!
    name: String!
    numberOfPages: Int!
    publishedAt: String!
    description: String!
    students: [Student]!
  }
`;
