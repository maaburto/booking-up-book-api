import { Student } from '../../../student/core';
import { NewBookDto, Book } from '../../core';
import { BookFeatures, BookStudentFeatures } from '../../features';

export class BookResolver {
  constructor(private bookFeatures: BookFeatures, private bookStudentFeatures: BookStudentFeatures) {}

  private getOneBook(bookId: string): Promise<Book | null> {
    return this.bookFeatures.getOneBook(bookId);
  }

  private getBooks(): Promise<Book[]> {
    return this.bookFeatures.getBooks();
  }

  private addBook(newCommittedBook: NewBookDto): Promise<Book> {
    return this.bookFeatures.addBook(newCommittedBook);
  }

  private deleteBook(bookId: string): Promise<boolean> {
    return this.bookFeatures.deleteBook(bookId);
  }

  private getStudentsByBookId(bookId: string): Promise<Student[]> {
    return this.bookStudentFeatures.getStudentsByBookId(bookId);
  }

  resolve() {
    return {
      Query: {
        book: async (_: any, { id }: { id: string }) => {
          const book = await this.getOneBook(id);
          return book;
        },

        books: async () => {
          const books = await this.getBooks();
          return books;
        },
      },

      Mutation: {
        addBook: (_: any, { book }: { book: NewBookDto }, context: any, info: any) => {
          return this.addBook(book);
        },

        deleteBook: async (_: any, { id }: { id: string }) => await this.deleteBook(id),
      },

      Book: {
        students: async ({ id }: { id: string }) => {
          return this.getStudentsByBookId(id);
        },
      },
    };
  }
}
