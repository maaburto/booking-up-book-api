export { default as bookTypeDef } from './book.graphql';

import { BookFeatures, BookStudentFeatures } from '../../features';
import { BookRepository } from '../repository';
import { BookResolver } from './book.resolver';

const bookRepository = new BookRepository();
const bookStudentFeatures = new BookStudentFeatures(bookRepository);
const bookFeatures = new BookFeatures(bookRepository);
const bookResolver = new BookResolver(bookFeatures, bookStudentFeatures);

// Once Dependencies has been fulfilled, it's time to run the route
const bookResolve = bookResolver.resolve();
export { bookResolve };
