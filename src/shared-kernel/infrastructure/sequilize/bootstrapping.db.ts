import { DataTypes, Sequelize } from 'sequelize';
import { bookModelName, bookAttributes } from '../../../book/infrastructure/repository';
import { studentModelName, studentAttributes } from '../../../student/infrastructure/repository';

const stringPGConnection: string = process.env.DB_CONNECTION ?? '';
const sequelize = new Sequelize(stringPGConnection, {
  dialect: 'postgres',
});

// `sequelize.define` also returns the model
const BookSchemaModel = sequelize.define(bookModelName, bookAttributes, {
  paranoid: true,
});

const StudentSchemaModel = sequelize.define(studentModelName, studentAttributes, {
  paranoid: true,
});

const BooksStudentsSchemaModel = sequelize.define('Books_Students', {
  id: {
    type: DataTypes.UUID,
    primaryKey: true,
    allowNull: false,
    defaultValue: Sequelize.literal('uuid_generate_v4()'),
  },
  createdAt: {
    type: DataTypes.DATE,
    defaultValue: Sequelize.literal('NOW()'),
  },
  updatedAt: {
    type: DataTypes.DATE,
    defaultValue: Sequelize.literal('NOW()'),
  },
});

BookSchemaModel.belongsToMany(StudentSchemaModel, {
  through: BooksStudentsSchemaModel,
});
StudentSchemaModel.belongsToMany(BookSchemaModel, {
  through: BooksStudentsSchemaModel,
});

export default sequelize;
export { BookSchemaModel, StudentSchemaModel, BooksStudentsSchemaModel };
