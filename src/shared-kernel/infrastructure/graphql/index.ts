import { gql } from 'apollo-server-express';
import { makeExecutableSchema } from '@graphql-tools/schema';
import { bookResolve, bookTypeDef } from '../../../book/infrastructure/graphql';
import { studentTypeDef, studentResolve } from '../../../student/infrastructure/graphql';

const link = gql`
  type Query {
    _: Boolean
  }
  type Mutation {
    _: Boolean
  }
  type Subscription {
    _: Boolean
  }
`;

const schema = makeExecutableSchema({
  typeDefs: [link, bookTypeDef, studentTypeDef],
  resolvers: [bookResolve, studentResolve],
});

export default schema;
