import Joi from '@hapi/joi';

export const dateValidation = Joi.string().regex(
  /^(19[0-9]{2}|2[0-9]{3})-(0[1-9]|1[012])-([123]0|[012][1-9]|31)$/
);
