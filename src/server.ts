import * as dotenv from 'dotenv';
dotenv.config();

import express from 'express';
import { ApolloServer, Config, ExpressContext } from 'apollo-server-express';
import { createServer } from 'http';
import cors from 'cors';
import { initSequilizeToDatabase, graphqlSchema } from './shared-kernel';

const ROOT_GRAPHQL = process.env.ROOT_GRAPHQL;
const PORT = process.env.PORT;

initSequilizeToDatabase
  .sync()
  .then(() => {
    console.log('Connection with DB has been established successfully.');
  })
  .catch((error) => {
    console.error('Unable to connect to the database:', error);
  });

const app = express();

app.use('*', cors());

startServer({
  schema: graphqlSchema,
})
  .then((server) => {
    server.applyMiddleware({ app, path: `/${ROOT_GRAPHQL}` });
  })
  .then(() => {
    const httpServer = createServer(app);
    httpServer.listen({ port: PORT }, (): void =>
      console.log(`🚀 Apollo Server on http://localhost:${PORT}/${ROOT_GRAPHQL}`)
    );
  });

async function startServer(config: Config<ExpressContext>) {
  const server = new ApolloServer(config);
  await server.start();

  return server;
}
